﻿CREATE TABLE [dbo].[Articulo] (
    [idArticulo]     INT             IDENTITY (1, 1) NOT NULL,
	[codigo] NCHAR(50) NOT NULL, 
    [descripcion]    VARCHAR (100)   NULL,
    [precioUnitario] DECIMAL (10, 2) NULL,
    [existencia]     INT             NULL,
    [estado]         VARCHAR (10)    NULL,    
    PRIMARY KEY CLUSTERED ([idArticulo] ASC)
);

