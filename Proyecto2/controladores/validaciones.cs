﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto2.controladores
{
    class validaciones
    {
        public void ValidarLetras(KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsWhiteSpace(e.KeyChar) || e.KeyChar == 46)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Ingreso Invalidos", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ValidarNumeros(KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar) || e.KeyChar == 46)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Ingreso Invalido", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
