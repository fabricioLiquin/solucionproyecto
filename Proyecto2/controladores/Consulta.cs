﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Windows.Forms;

namespace Proyecto2.controladores
{
    class Consulta
    {

        private SqlConnection conexion = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Repos\solucionProyecto\Proyecto2\Database1.mdf;Integrated Security=True");
        private DataSet ds;


        public DataTable MostrarDatos()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("select * from Articulo", conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "Articulo");
            conexion.Close();
            return ds.Tables["Articulo"];
        }

        public DataTable BuscarPorID_Producto(string idArt)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select * from Articulo where idArticulo like '%{0}%'", idArt), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "Articulo");
            conexion.Close();
            return ds.Tables["Articulo"];
        }

        public DataTable BuscarPorDescripcion(string descripcion)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select * from Articulo where descripcion like '%{0}%'", descripcion), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "Articulo");
            conexion.Close();
            return ds.Tables["Articulo"];
        }

        public bool Insertar(string idArt, string codigo,string desc, string PreUni, string Exist, string Estado)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("insert into Articulo(codigo, descripcion, precioUnitario, existencia, estado) values (@codigo, @descripcion, @precioUnitario, @existencia, @estado)", conexion);
            cmd.Parameters.Add("@codigo", SqlDbType.VarChar);
            cmd.Parameters.Add("@descripcion", SqlDbType.VarChar);
            cmd.Parameters.Add("@precioUnitario", SqlDbType.Decimal);
            cmd.Parameters.Add("@existencia", SqlDbType.Int);
            cmd.Parameters.Add("@estado", SqlDbType.VarChar);
<<<<<<< HEAD
=======

>>>>>>> 1d1a6db25a614192796e6c4ffcd4096da3809e5e

            cmd.Parameters["@codigo"].Value = codigo;
            cmd.Parameters["@descripcion"].Value = desc;
            cmd.Parameters["@precioUnitario"].Value = PreUni;
            cmd.Parameters["@existencia"].Value = Exist;
            cmd.Parameters["@estado"].Value = Estado;

            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }

        public bool Eliminar(string idArticulo)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("delete from Articulo where idArticulo =" + idArticulo, conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }

        public bool Actualizar(string idArt, string codigo, string desc, string PreUni, string Exist, string Estado)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("update Articulo set descripcion=@descripcion, precioUnitario=@precioUnitario, existencia=@existencia, estado=@estado where idArticulo =" + idArt, conexion);

            cmd.Parameters.Add("@codigo", SqlDbType.VarChar);
            cmd.Parameters.Add("@descripcion", SqlDbType.VarChar);
            cmd.Parameters.Add("@precioUnitario", SqlDbType.Decimal);
            cmd.Parameters.Add("@existencia", SqlDbType.Int);
            cmd.Parameters.Add("@estado", SqlDbType.VarChar);

            cmd.Parameters["@codigo"].Value = codigo;
            cmd.Parameters["@descripcion"].Value = desc;
            cmd.Parameters["@precioUnitario"].Value = PreUni;
            cmd.Parameters["@existencia"].Value = Exist;
            cmd.Parameters["@estado"].Value = Estado;

            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }

        public bool InsertarVenta( string fecha, string total)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("insert into Venta( fecha, total) values (@fecha, @total)", conexion);
            cmd.Parameters.Add("@fecha", SqlDbType.DateTime);
            cmd.Parameters.Add("@total", SqlDbType.Decimal);
            


            cmd.Parameters["@fecha"].Value = fecha;
            cmd.Parameters["@total"].Value = total;
            

            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }

        public bool InsertarLineaVenta(string idLineaVenta, string cantidad, string subTotal, string id_venta, string id_Art)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand("insert into LineaVenta( cantidad, subTotal, idVenta,idArticulo) values (@cantidad, @subTotal, @idVenta, @idArticulo)", conexion);
            cmd.Parameters.Add("@cantidad", SqlDbType.Int);
            cmd.Parameters.Add("@subTotal", SqlDbType.Decimal);
            cmd.Parameters.Add("@idVenta", SqlDbType.Int);
            cmd.Parameters.Add("@idArticulo", SqlDbType.Int);



            cmd.Parameters["@cantidad"].Value = cantidad;
            cmd.Parameters["@subTotal"].Value = subTotal;
            cmd.Parameters["@idVenta"].Value = id_venta;
            cmd.Parameters["@idArticulo"].Value = id_Art ;




            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }
    }
}