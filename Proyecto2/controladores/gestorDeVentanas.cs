﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto2.controladores
{
    class gestorDeVentanas
    {
        static vistaMenuPrincipal vMenPri = new vistaMenuPrincipal();
        static vistaVenta vVenta = new vistaVenta();
        static vistaProducto vProducto = new vistaProducto();
        

        public static void mostrarVistaMenuPrincipal()
        {
            vMenPri.Visible = true;
            vVenta.Visible = false;
            vProducto.Visible = false;
        }
        public static void mostrarVistaVenta()
        {
            vVenta.Visible = true;
            vProducto.Visible = false;
            vMenPri.Visible = false;
            
        }

        public static void mostrarVistaProducto()
        {
            vProducto.Visible = true;
            vVenta.Visible = false;
            vMenPri.Visible = false;
        }

        public static void cerrarAplicacion()
        {
            Environment.Exit(0);
        }
        public static System.Windows.Forms.Form
          ObtenerMenuPrincipal()
        {
            return vMenPri;
        }

    }
}
