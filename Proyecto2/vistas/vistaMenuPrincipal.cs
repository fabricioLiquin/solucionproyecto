﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto2
{
    public partial class vistaMenuPrincipal : Form
    {
        public vistaMenuPrincipal()
        {
            InitializeComponent();
        }

        private void ventaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controladores.gestorDeVentanas.mostrarVistaVenta();
        }

        private void productoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controladores.gestorDeVentanas.mostrarVistaProducto();
        }

        private void cerrarProgramaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controladores.gestorDeVentanas.cerrarAplicacion();
        }

        private void vistaMenuPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
