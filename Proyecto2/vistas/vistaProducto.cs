﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto2
{
    public partial class vistaProducto : Form
    {
        public vistaProducto()
        {
            InitializeComponent();
        }

        controladores.validaciones  val = new controladores.validaciones();
        controladores.Consulta consulta = new controladores.Consulta();

        private void Limpiar(Panel panelDatos, GroupBox gbBuscar)
        {
            // Revisar todos los textbox del formulario
            foreach (Control oControls in panelDatos.Controls)
            {
                if (oControls is TextBox)
                {
                    // Eliminar el texto del TextBox
                    oControls.Text = "";

                }
                else if (oControls is ComboBox)
                {
                    oControls.Text = null;

                }

                //Revisar todos los textbox del groupBox
                foreach (Control gb in gbBuscar.Controls)
                {
                    if (gb is TextBox)
                    {
                        gb.Text = "";
                    }
                }
                
            }

        }

       private bool validar()
        {
            if (txtCodigo.Text.Trim() == string.Empty)
            {
                errorIcono.SetError(txtCodigo, "Ingrese Codigo");
                return false;
            }
            else
            {

                if (txtDescripcion.Text.Trim()==string.Empty)
                {
                    errorIcono.Clear();
                    errorIcono.SetError(txtDescripcion, "Ingrese Descripcion");
                    return false;
                }
                else
                {
                    if (txtPreUni.Text.Trim()==string.Empty)
                    {
                        errorIcono.Clear();
                        errorIcono.SetError(txtPreUni, "Ingrese Precio Unitario");
                        return false;
                    }
                    else
                    {
                        if (txtExistencia.Text.Trim()==string.Empty)
                        {
                            errorIcono.Clear();
                            errorIcono.SetError(txtExistencia, "Ingrese Existencia");
                            return false;
                        }
                        else
                        {
                            if (cbEstado.Text.Trim()==string.Empty)
                            {
                                errorIcono.Clear();
                                errorIcono.SetError(cbEstado,"Ingrese Estado");
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                            
                        }
                    }

                }
            }

        }
        private void vistaProducto_Load(object sender, EventArgs e)
        {
            dgvProducto.DataSource = consulta.MostrarDatos();

            

            dgvProducto.ColumnHeadersDefaultCellStyle.BackColor = Color.AliceBlue;

            dgvProducto.Columns[0].HeaderText = "ID_Producto";
            dgvProducto.Columns[1].HeaderText = "Codigo";
            dgvProducto.Columns[2].HeaderText = "Descripcion";
            dgvProducto.Columns[3].HeaderText = "Precio_Unitario";
            dgvProducto.Columns[4].HeaderText = "Existencia";
            dgvProducto.Columns[5].HeaderText = "Estado";
            
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar(panelDatos,gbBuscar);
        }

        private void txtDescripcion_KeyPress(object sender, KeyPressEventArgs e)
        {
         
        }

        private void txtPreUni_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.ValidarNumeros(e);
        }

        private void txtCant_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.ValidarNumeros(e);
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdProd.Text.Trim()))
            {
                if (validar())
                {
                    consulta.Insertar(txtIdProd.Text, txtCodigo.Text, txtDescripcion.Text,txtPreUni.Text,txtExistencia.Text, cbEstado.Text);
                    MessageBox.Show("Se Registro un nuevo producto", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar(panelDatos,gbBuscar);
                    dgvProducto.DataSource = consulta.MostrarDatos();
                }
            }
               
        }

        private void txtDescripcion_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void txtPreUni_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void txtCant_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void cbEstado_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdProd.Text.Trim()))
            {
                errorIcono.Clear();
                MessageBox.Show("No hay registro seleccionado para eliminar", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                if (MessageBox.Show("Estas seguro de eliminar este registro ?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    consulta.Eliminar(txtIdProd.Text);
                    Limpiar(panelDatos,gbBuscar);
                    MessageBox.Show("Registro eliminado", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIdProd.Text.Trim()))
            {
                errorIcono.Clear();
                MessageBox.Show("No hay registro seleccionado para actualizar", "ATENCION!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                if (MessageBox.Show("Desea actualizar el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    consulta.Actualizar(txtIdProd.Text, txtCodigo.Text, txtDescripcion.Text, txtPreUni.Text, txtExistencia.Text, cbEstado.Text);
                    dgvProducto.DataSource = consulta.MostrarDatos();
                    Limpiar(panelDatos, gbBuscar);
                    MessageBox.Show("Registro Actualizado", "MENSAJE", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtBuscar.Text != "")
            {
                if (rbtnID.Checked)
                {
                    dgvProducto.DataSource = consulta.BuscarPorID_Producto(txtBuscar.Text);
                }
                else
            if (rbtnDesc.Checked)
                {
                    dgvProducto.DataSource = consulta.BuscarPorDescripcion(txtBuscar.Text);
                }

            }
            else
            {
                dgvProducto.DataSource = consulta.MostrarDatos();
            }
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (rbtnID.Checked)
            {
                val.ValidarNumeros(e);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            controladores.gestorDeVentanas.mostrarVistaMenuPrincipal();
        }

        private void btnMosTod_Click(object sender, EventArgs e)
        {
            dgvProducto.DataSource = consulta.MostrarDatos();
        }



<<<<<<< HEAD
=======
        private void txtBuscar_TextChanged_1(object sender, EventArgs e)
        {
        }
        
        
>>>>>>> 1d1a6db25a614192796e6c4ffcd4096da3809e5e
        private void dgvProducto_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            errorIcono.Clear();
            
            DataGridViewRow fila = dgvProducto.Rows[e.RowIndex];

            fila.DefaultCellStyle.BackColor = Color.LightGray;

            txtIdProd.Text = Convert.ToString(fila.Cells[0].Value);
            txtCodigo.Text = Convert.ToString(fila.Cells[1].Value);
            txtDescripcion.Text = Convert.ToString(fila.Cells[2].Value);
            txtPreUni.Text = Convert.ToString(fila.Cells[3].Value);
            txtExistencia.Text = Convert.ToString(fila.Cells[4].Value);
            cbEstado.Text= Convert.ToString(fila.Cells[5].Value);
            

        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.ValidarNumeros(e);
        }
    }
}
