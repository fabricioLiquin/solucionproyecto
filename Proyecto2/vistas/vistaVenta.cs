﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto2
{
    public partial class vistaVenta : Form
    {

        private SqlConnection conexion = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Repos\solucionProyecto\Proyecto2\Database1.mdf;Integrated Security=True");
        private DataSet ds;
        decimal total = 0;
        DataTable tablaLineaVenta = new DataTable("Agregar Linea de Venta");


        public vistaVenta()
        {
            InitializeComponent();
            iniciarTablaLineaVenta();
        }

        private void iniciarTablaLineaVenta()
        {
            
            tablaLineaVenta.Columns.Add(new DataColumn("Codigo"));
            tablaLineaVenta.Columns.Add(new DataColumn("Descripcion"));
            tablaLineaVenta.Columns.Add(new DataColumn("Cant."));
            tablaLineaVenta.Columns.Add(new DataColumn("Precio U."));
            tablaLineaVenta.Columns.Add(new DataColumn("SubTotal"));
        }

        controladores.validaciones val = new controladores.validaciones();

        private void Limpiar(Panel panelDatos, Panel panelDatos2, GroupBox gbBuscar)
        {
            // Revisar todos los textbox del formulario
            foreach (Control oControls in panelDatos.Controls)
            {
                if (oControls is TextBox)
                {
                    // Eliminar el texto del TextBox
                    oControls.Text = "";

                }
               //Revisar todos los textbox del groupBox
                foreach (Control Controls in panelDatos2.Controls)
                {
                    if (Controls is TextBox)
                    {
                        Controls.Text = "";
                    }
                }
                foreach (Control gb in gbBuscar.Controls)
                {
                    if (gb is TextBox)
                    {
                        gb.Text = "";
                    }
                }

            }

        }

        private bool validarLineaVenta()
        {
            if (txtCodigo.Text.Trim() == string.Empty)
            {
                errorIcono.SetError(txtCodigo, "Seleccione un producto");
                return false;
            }
            else
            {
                if (txtCant.Text.Trim() == string.Empty)
                {
                    errorIcono.Clear();
                    errorIcono.SetError(txtCant, "Ingrese Cantidad");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private bool validarVenta()
        {
            if (txtIngreso.Text.Trim()==string.Empty)
            {
                errorIcono.SetError(txtIngreso,"Ingrese el monto Recibido");
                return false;
            }
            return true;            
        }


       
        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.ValidarNumeros(e);
        }

        private void txtIngreso_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.ValidarNumeros(e);
        }

        private void btnSalir_Click_1(object sender, EventArgs e)
        {
            controladores.gestorDeVentanas.mostrarVistaMenuPrincipal();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar(panelDatos,panelDatos2,gbBuscar);
            errorIcono.Clear();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (validarLineaVenta())
            {
                agregarProductoALineaVenta();
                LimpiarDatosArticulo(panelDatos);
                altaLineVenta();     
            }
        }

        private void LimpiarDatosArticulo(Panel panelDatos)
        {
            foreach (Control oControls in panelDatos.Controls)
            {
                if (oControls is TextBox)
                {
                    // Eliminar el texto del TextBox
                    oControls.Text = "";

                }
            }
        }

        private void agregarProductoALineaVenta()
        {          
                      
            DataRow row = tablaLineaVenta.NewRow();
            row["Codigo"] = txtCodigo.Text;
            row["Descripcion"] = txtDescripcion.Text;
            row["Cant."] = txtCant.Text;
            row["Precio U."] = txtPreUni.Text;
            row["SubTotal"] = txtSubTotal.Text;
            tablaLineaVenta.Rows.Add(row);

            dgvLineaVenta.DataSource = tablaLineaVenta;

            actualizarTotal(Convert.ToDecimal(txtSubTotal.Text));
            
        }

        private void actualizarTotal(decimal value)
        {

            total = total + value;
            txtTotal.Text = ""+total;

        }

        private void altaLineVenta()
        {
            
        }

        private void txtCant_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.ValidarNumeros(e);
        }

        controladores.Consulta consulta = new controladores.Consulta();

        private void vistaVenta_Load(object sender, EventArgs e)
        {
            dgvProducto.DataSource = consulta.MostrarDatos();
            lblFechaVenta.Text = DateTime.Now.ToString("dd/MM/yyyy");


        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (validarVenta())
            {
                consulta.InsertarVenta(lblFechaVenta.Text, txtTotal.Text);
                MessageBox.Show("Venta Exitosa");
            }
        }

        private void txtIdProd_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void txtCant_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void txtIngreso_TextChanged(object sender, EventArgs e)
        {
            errorIcono.Clear();
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (rbtnID.Checked)
            {
                val.ValidarNumeros(e);
            }
        }

        private void btnMosTod_Click(object sender, EventArgs e)
        {
            //controladores.Consulta consulta = new controladores.Consulta();

            mostrarDatos();
        }

        private void mostrarDatos()
        {            
                conexion.Open();
                SqlCommand cmd = new SqlCommand("select * from Articulo", conexion);
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ds = new DataSet();
                ad.Fill(ds, "Articulo");
                conexion.Close();
                dgvProducto.DataSource = ds.Tables["Articulo"];
            
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtBuscar.Text != "")
            {
                if (rbtnID.Checked)
                {
                    buscarPorID_Producto(txtBuscar.Text);
                }
                else
            if (rbtnDesc.Checked)
                {
                    buscarPorDescripcion(txtBuscar.Text);
                }

            }
            else
            {
                mostrarDatos();
            }
        }

        private void buscarPorDescripcion(string txt)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select * from Articulo where descripcion like '%{0}%'", txt), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "Articulo");
            conexion.Close();
            dgvProducto.DataSource = ds.Tables["Articulo"];
        }

        private void buscarPorID_Producto(string txt)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select * from Articulo where codigo like '%{0}%'", txt), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "Articulo");
            conexion.Close();
            dgvProducto.DataSource = ds.Tables["Articulo"];
        }

        private void dgvProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtCodigo.Text = Convert.ToString(dgvProducto.CurrentRow.Cells[1].Value);
            txtDescripcion.Text = Convert.ToString(dgvProducto.CurrentRow.Cells[2].Value);
            txtPreUni.Text = Convert.ToString(dgvProducto.CurrentRow.Cells[3].Value);            
        }

        private void txtCant_KeyUp(object sender, KeyEventArgs e)
        {
            if (!txtCant.Text.Equals(""))
            {
                int cant = int.Parse(txtCant.Text);
                decimal preVen = decimal.Parse(txtPreUni.Text);
                txtSubTotal.Text = Convert.ToString(cant * preVen);
            } else
            {
                txtSubTotal.Text = "";
                e.Handled = false;
            }                       
            
        }

        private void txtSubTotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvLineaVenta.CurrentRow==null)
            {
                return;
            }
            else
            {
                restarTotal(Convert.ToDecimal(dgvLineaVenta.CurrentRow.Cells[4].Value));
                dgvLineaVenta.Rows.Remove(dgvLineaVenta.CurrentRow);
            }

            
        }

        private void restarTotal(decimal v)
        {
            total = total - v;
            txtTotal.Text = Convert.ToString(total);
        }
    }
}
